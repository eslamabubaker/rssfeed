<?php
header("Content-Type: text/xml");

include 'dbconnection.php';
$conn->set_charset("utf8");

$rssfeed = '<?xml version="1.0" encoding="UTF-8"?>'."\r\n";
$rssfeed .= '<rss version="2.0">'."\r\n";
$rssfeed .= '<channel>'."\r\n";
$rssfeed .= '<title>'.$title.'</title>'."\r\n";
$rssfeed .= '<link>'.$website.'</link>'."\r\n";
$rssfeed .= '<description>'.$description.'</description>'."\r\n";
$rssfeed .= '<language>'.$lang.'</language>'."\r\n";



$query = "SELECT * FROM videos ORDER BY created_at DESC limit 20";
$result = mysqli_query($conn,$query) or die ("Could not execute query");

while($row = mysqli_fetch_array($result)) {
    extract($row);

    $rssfeed .= '<item>'."\r\n";
    $rssfeed .= '<title>' . strip_tags($title) . '</title>'."\r\n";
    $rssfeed .= '<description>' . strip_tags($description) . '</description>'."\r\n";
    if($image!=null && $image !='') {
        $rssfeed .= '<image>' . "\r\n";
        $rssfeed .= '<url>' . $img_url . $image . '</url>' . "\r\n";
        $rssfeed .= '<link>' . $base_url_videos . $id . "-" . strip_tags($seo_name) . '</link>' . "\r\n";
        $rssfeed .= '</image>' . "\r\n";
    }
    $rssfeed .= '<link>' . $img_url.$source. '</link>'."\r\n";
    $rssfeed .= '<pubDate>' . date("D, d M Y H:i:s O", strtotime($created_at)) . '</pubDate>'."\r\n";
    $rssfeed .= '</item>'."\r\n";
}

$rssfeed .= '</channel>'."\r\n";
$rssfeed .= '</rss>'."\r\n";

echo $rssfeed;

?>