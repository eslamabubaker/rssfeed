<?php
header("Content-Type: text/xml");


include 'dbconnection.php';
$conn->set_charset("utf8");

$rssfeed = '<?xml version="1.0" encoding="UTF-8" ?>'."\r\n";
$rssfeed .= '<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
 xmlns="http://purl.org/rss/1.0/"
 xmlns:dc="http://purl.org/dc/elements/1.1/"
>'."\r\n";
$rssfeed .= '<channel rdf:about="'.$website.'/rss/news.rss">'."\r\n";
$rssfeed .= '<title>'.$title.'</title>'."\r\n";
$rssfeed .= '<link>'.$website.'</link>'."\r\n";
$rssfeed .= '<description>'.$description.'</description>'."\r\n";
$rssfeed .= '<language>'.$lang.'</language>'."\r\n";



$query = "SELECT * FROM articles ORDER BY created_at DESC limit 20";
$result = mysqli_query($conn,$query) or die ("Could not execute query");

while($row = mysqli_fetch_array($result)) {
    extract($row);

    $rssfeed .= '<item rdf:about="' . $base_url_articals. $id.'-' . strip_tags($seo_name). '">'."\r\n";
    $rssfeed .= '<title>' . strip_tags($title) . '</title>'."\r\n";
    if($image!=null && $image !='') {
        $rssfeed .= '<image rdf:about="' . $img_url . $image . '" >' . "\r\n";
        $rssfeed .= '<url>' . $img_url . $image . '</url>' . "\r\n";
        $rssfeed .= '<link>' . $base_url_articals . $id . "-" . strip_tags($seo_name) . '</link>' . "\r\n";
        $rssfeed .= '</image>' . "\r\n";
    }
    $rssfeed .= '<description>' . strip_tags($description) . '</description>'."\r\n";
    $rssfeed .= '<link>' . $base_url_articals. $id . "-" . strip_tags($seo_name). '</link>'."\r\n";
    $rssfeed .= '<dc:date>' . date("Y - M - d", strtotime($created_at)) . '</dc:date>'."\r\n";
    $rssfeed .= '</item>'."\r\n";
}

$rssfeed .= '</channel>'."\r\n";
$rssfeed .= '</rdf:RDF>'."\r\n";

echo $rssfeed;

?>